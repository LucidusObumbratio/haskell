module Exercises where
--
import Data.List
import Data.Char
--

{-LECTURE 01-} 

-- EXERCISE 01========================================================================
ex111 = concat3
concat3 a b c
	|length b < 2 = a ++ c
	| otherwise = a ++ b ++ c

ex112 = showSalary
showSalary a b = if (a + b) < 0 then "Your salary is terrrible..."
	else "Salary: " ++ show (a + b)

{-LECTURE 02-} 

-- EXERCISE 01========================================================================

-- Define a function that returns a list without the first three elements and
-- last three elements.
ex211 a
	| length a > 5 = drop 3 (take (length a - 3) a)
	| otherwise = []

-- Define a function 'initals s1 s2' that takes a person's name and a surname 
-- as input and returns a string consisting of person's initials.
-- initials "James" "Bond" => "J. B."
ex212 = initials
initials name surname
	| not (null name) && not (null surname) = [head name] ++ ". " ++ [head surname] ++ "."
	| otherwise = ""

-- Define a function that concatenates two strings, so that the longest string
-- always comes first.
ex213 a b
	| length a > length b = a ++ b
	| otherwise = b ++ a

-- Define a function 'safeHead' that returns an empty list if 'l' is an empty
-- list, otherwise it returns its first element wrapped inside a singleton list.
ex214 = safeHead
safeHead l 
	| null l = []
	| otherwise = [head l]

-- Define a function 'hasDuplicates' that checks whether a list contains
-- duplicate elements (use 'nub').
ex215 = hasDuplicates
hasDuplicates :: [String] -> Bool
hasDuplicates str = length str /= length (nub str)

-- EXERCISE 02========================================================================

-- Redefine 'doublesFromTo' so that it also works when b<a.
ex221 = doublesFromTo
doublesFromTo a b
	| a < b = [x*2 | x <- [a..b]]
	| otherwise = reverse [x*2 | x <- [b..a]]

-- Redefine 'ceasarCode n xs' so that it shifts all letters a specified number 
-- of positions 'n', converts all input to lowercase, and ensures that letters 
-- remain within the ['a'..'z'] interval.
ex222 = caesarCode
caesarCode n xs = [ chr ((ord (toLower x) + n - 97) `mod` 26 + 97) | x <- xs ]

-- EXERCISE 03========================================================================

-- Define 'letterCount' that computes the total number of letters in a string,
-- thereby ignoring the whitespaces and all words shorter than three letters.
-- You can use 'totalLength'.

lengths xss = [length xs | xs <- xss]
totalLength xss = sum $ lengths xss

ex231 = letterCount
letterCount str = totalLength [x | x <- words str, length x > 2]

-- Redefine 'isPalindrome' so that it's case insensitive and works correctly 
-- for strings that contain whitespaces.

lowercase str = [ toLower x | x <- str ]
ex232 = isPalindrome
isPalindrome :: String -> Bool
isPalindrome str = if lowercase str == (reverse (lowercase str)) then True else False

-- Define 'flipp xss' that takes a list of lists, reverts each individual list,
-- and concatenates all of them, but in the reverse order.
-- flip ["water","is","warm"] -> "mrawsiretaw"

ex233 = flipp
flipp l = concat (reverse [ x | x <- [ reverse y | y <- l ] ])

-- EXERCISE 04========================================================================

-- Define 'inCircle r x y' that returns the coordinates of all points within
-- the ([-10..10],[-10..10]) interval that fall inside a circle of radius
-- 'r' with center '(x,y)'.
inCircleBasic r x y = [(px, py) | px <- [(-10), (-9)..10], py <- [(-10), (-9)..10], r*r >= (px - x)*(px - x) + (py - y)*(py - y)]

-- Redefine the function so that it takes the resolution of the grid as an 
-- additional argument.
ex241 = inCircle
inCircle r x y grid = [(px, py) | px <- [(-10.0), (grid-10.0)..10.0], py <- [(-10.0), (grid-10.0)..10.0], r*r >= (px - x)*(px - x) + (py - y)*(py - y)]

-- Define 'steps xs' that, given a list xs=[x1,x2,..], generates the pairs
-- [(x1,x2),(x2,x3),...]. Hint: have a look at 'pairs5'.
ex242 = steps
steps xs = zip xs (tail xs)

-- EXERCISE 05========================================================================

-- Define 'indices x xs' that returns the indices of element 'x' in list 'xs'
-- (if 'x' appears multiple times, there will be a number of such indices).
-- indices 'a' "alphabet" => [0, 4]
ex251 = indices
indices :: Char -> String -> [Int]
indices x xs = [ w1 | (w1, w2) <- zip [0..] xs, x == w2 ]

-- Define 'showLineNumbers s' that prefixes all lines from string 's' with a
-- line number.
-- showLineNumbers 1 first line\n2 second line\n"
ex252 = showLineNumbers
showLineNumbers str = concat [show (fst x) ++ " " ++ snd x ++ "\n" | x <- zip [1..] (lines str)]

-- Define 'haveAlignment xs ys' that returns 'True' if 'xs' and 'ys' have
-- any identical elements that are aligned (appear at the same position in
-- both lists)
ex253 = haveAlignment
haveAlignment :: String -> String -> Bool
haveAlignment xs ys = not (null [ x | x <- (zip xs ys), (fst x) == (snd x) ])

-- Define 'common xs ys' that returns the aligned subsequences.
-- haveAlignment "water" "fire" => True
-- common "witer" "fire" => "ie"
ex254 = common
common :: String -> String -> String
common xs ys = [ fst x | x <- (zip xs ys), (fst x) == (snd x)]

{-LECTURE 03-}

-- EXERCISE 01========================================================================

-- Without using the ':t' command, determine the types of the following 
-- functions:

foo10 w = [x ++ y | x <- lines w, y <- lines w] -- String -> [String]
foo11 w = [(x,y) | x <- lines w, y <- lines w] -- Strng -> [(String, String)]
foo12 w = [y : x | x <- lines w, y <- w] -- String -> [String]
foo13 w = [(y:x, w) | x <- lines w, y <- w] -- String -> [(String, String)]
foo14 w = [(x, x=='a') | x <- w ] -- String -> [(Char, Bool)]
foo15 s = tail [ c | c <- s, isLower c ] -- String -> String
foo16 s = zip [ c | c <- s, isLower c ] "Haskell" -- String -> [(Char, Char)]
foo17 n c = reverse $ drop n $ c : "Haskell" -- Int -> Char -> String
foo18 xs = last $ words xs -- String -> String
foo19 x z = x : 'y' : z -- Char -> String -> String

-- EXERCISE 02========================================================================

-- Without using the ':t' command, determine the types of the following 
-- functions:

foo20 xs = tail xs ++ [head xs] -- [a] -> [a]
foo21 xs = (head xs, tail xs) -- [a] -> (a,[a])
foo22 x xs = x:xs -- a -> [a] -> [a]
foo23 l = init $ tail l -- [a] -> [a]
foo24 xss ys = concat xss ++ ys -- [a] -> [a] -> [a]
foo25 xss ys = (head $ concat xss, head ys) -- [a] -> [b] -> (a, b)
foo26 xs = head $ concat $ concat xs -- [[a]] -> a
foo27 cs = [[c1,c2] | c1 <- cs, c2 <- cs] -- [a] -> [[a]]
foo28 cs = [concat [c1,c2] | c1 <- cs, c2 <- cs] -- [a] -> [a] (?)
foo29 cs = concat [[c1,c2] | c1 <- cs, c2 <- cs] -- [a] -> [a]

-- EXERCISE 03========================================================================

-- Without using the ':t' command, determine the types of the following 
-- functions:

foo30 x ys = if x==head ys then x else last ys -- a -> [a] -> a
foo31 x ys = if x < head ys then x else last ys -- a -> [a] -> a
foo32 xs yss = if xs==head yss then head xs else last xs -- [a] -> [[a]] -> [a]
foo33 x ys = if x then zip [1..9] ys else [] -- Bool -> [a] -> [(b,a)]
foo34 w = zip [0..] (lines w) -- String -> [(a, String)]
foo35 x y = if odd x then y else x / 10 -- a -> a -> a
foo36 xs = sort xs == xs -- [a] -> Bool
foo37 x xs = show x ++ (show $ concat xs) -- a -> [a] -> String
foo38 xs = sum $ concat xs -- [a] -> a
foo39 xs yss = sum $ [min x y | x <- xs, ys <- yss, y <- ys] -- [a] -> [[a]] -> a





