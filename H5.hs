module Homework where
--
import Data.List
--

-- Task 01

{-
A robot factory's test facility needs a program to verify robot movements.
The robots have three possible movements:
- turn right
- turn left
- advance
Robots are placed on a hypothetical infinite grid, facing a particular direction (north,
east, south, or west) at a set of fx,yg coordinates, e.g., f3,8g, with coordinates
increasing to the north and east.
The robot then receives a number of instructions, at which point the testing facility
verifies the robot's new position, and in which direction it is pointing.
The letter-string "RAALAL" means:
- Turn right
- Advance twice
- Turn left
- Advance once
- Turn left yet again
Say a robot starts at f7, 3g facing north. Then running this stream of instructions
should leave it at f9, 4g facing west.
To complete this exercise, you need to create the data type Robot, and implement
the following functions and data types:

data Bearing
data Robot

bearing :: Robot -> Bearing
coordinates :: Robot -> (Integer, Integer)
mkRobot :: Bearing -> (Integer, Integer) -> Robot

simulate :: Robot -> String -> Robot
-- simulate should be implemented in terms of fold

advance :: Bearing -> (Integer, Integer) -> (Integer, Integer)
turnLeft :: Bearing -> Bearing
turnRight :: Bearing -> Bearing
-}

{-
  Inspect type signatures of functions that you are supposed to implement
  and and from that information try to think of how Robot and Bearing 
  data types should look like.
-}

data Robot = Robot
 { direction :: Bearing
 , position :: (Integer, Integer)
 } deriving (Show)
data Bearing = NORTH | EAST | SOUTH | WEST deriving(Eq, Show)

mkRobot :: Bearing -> (Integer, Integer) -> Robot
mkRobot b (x,y) = Robot b (x,y)

bearing :: Robot -> Bearing
bearing r = direction r

myCop = mkRobot NORTH (0, 0)

coordinates :: Robot -> (Integer, Integer)
coordinates r = position r

{- It is MANDATORY to implement 'simulate' function in terms of fold -}
simulate :: Robot -> String -> Robot
simulate r xs = foldl execute r xs

execute r t
 | t == 'R' = r {direction = turnRight (bearing r)}
 | t == 'L' = r {direction = turnLeft (bearing r)}
 | otherwise = r {position = advance (bearing r) (coordinates r)}

advance :: Bearing -> (Integer, Integer) -> (Integer, Integer)
advance NORTH (x, y) = (x, y + 1)
advance SOUTH (x, y) = (x, y - 1)
advance EAST  (x, y) = (x + 1, y)
advance WEST  (x, y) = (x - 1, y)

turnLeft :: Bearing -> Bearing
turnLeft NORTH = WEST
turnLeft WEST  = SOUTH
turnLeft SOUTH = EAST
turnLeft EAST  = NORTH

turnRight :: Bearing -> Bearing
turnRight NORTH = EAST
turnRight EAST  = SOUTH
turnRight SOUTH = WEST
turnRight WEST  = NORTH

-- Task 02

{-
Determine if a triangle is equilateral, isosceles, scalene, or degenerate.
An Equilateral triangle has all three sides the same length.
An Isosceles triangle has at least two sides the same length. (It is sometimes
specified as having exactly two sides the same length, but for the purposes of this
exercise we'll say at least two.)
A Scalene triangle has all sides of different lengths.
A Degenerate triangle has zero area and looks like a single line. The sum of the
lengths of two sides equals that of the third.
For a shape to be a triangle at all, all sides have to be of length > 0, and the sum of
the lengths of any two sides must be greater than or equal to the length of the third
side. See Triangle Inequality.
All triangles that aren't any of the above are Illegal triangles.
Create a new data type Triangle and write a function that determines it's type.
Function should have following type signature:
-}

data TriangleType = EQUILATERAL | ISOSCELES | SCALENE | DEGENERATE | ILLEGAL deriving (Show)

triangleType :: (Ord a, Num a) => a -> a -> a -> TriangleType
triangleType a b c
 | not $ and[a + b >= c, a + c >= b, b + c >= a] = ILLEGAL
 | and[a == b, b == c] = EQUILATERAL
 | or[a + b == c, a + c == b, b + c == a] = DEGENERATE
 | or[a == b, a == c, b == c] = ISOSCELES
 | and[a /= b, a /= c, b /= c] = SCALENE

-- Task 03

{-
In this problem you will have to implement a utility for splitting lists on
sub lists. This can be quite useful for splitting delimited strings (and will be useful
in the next homework so make sure you solve this exercise :).
It's not very hard to solve this problem with standard recursions but this time you
have to use folds. Also, keep in mind that not all folds are equal. There is clearly
a better and worse choice so make sure you pick the right one for the job.
In case you need to return more than one value from the fold function, remember
that you can use tuples for that (even though it is not necessary in this case).
-}

{- some convenient test examples -}
-- splitter " > " " > " => ["", ""]
-- splitter " > " "123 > " => ["123", ""]
-- splitter " > " "123 > 456 > 789" => ["123", "456", "789"]

{-
  you don't have to bother with splitting on an empty list e.g.:
  splitter "" "abcde" => ["a", "b", "c", "d", "e"]
-}

{- It is MANDATORY to implement 'splitter' function in terms of fold -}
--splitter :: Eq a => [a] -> [a] -> [[a]]
--splitter delim xs = words $ fst $ foldl (\(str, d) x -> if (d ++ [x]) == delim then (newWord str d x, [x]) else (str ++ [x], addToDelim delim d x)) ("", "") xs

splitter :: Eq a => [a] -> [a] -> [[a]]
splitter delim xs = fst $ foldr (\x ((ys:yss), d) -> if (x:d) == delim 
 then ((([]):(newWord ys d):yss), []) 
 else ( (x:ys):yss , addToDelim delim d x)) 
 ([[]], []) xs

addToDelim origD actD x
 | (take lenActD origD) == actD = x:actD
 | otherwise = [x]
 where
 lenActD = length actD
 lenOrigD = length origD

newWord str d = drop (length d) str