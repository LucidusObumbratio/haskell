{-# LANGUAGE NoMonomorphismRestriction #-}
--
module Exercises where
--
import Control.Monad
import Control.Exception
import Control.Monad.Reader
--
import Data.Char
import Data.List
import Data.Monoid ( (<>) )
import Data.Foldable
import Data.Set hiding (filter, null, map, foldl)
import qualified Data.Map as M
--
import System.IO
import System.IO.Error
import System.Directory
import System.Environment
import System.FilePath
import System.Random
import System.FilePath
--
import Text.Read
--

{-LECTURE 10-} -- 
-- EXERCISE 01 =======================================================================


data Sex = Male | Female deriving (Show,Read,Ord,Eq)

data Person2 = Person2 {
  personId2 :: String,
  forename2 :: String,
  surname2  :: String,
  sex2      :: Sex,
  mother2   :: Maybe Person2,
  father2   :: Maybe Person2,
  partner2  :: Maybe Person2,
  children2 :: [Person2] } deriving (Show,Read,Eq,Ord)

john = Person2 "123" "John" "Doe" Male Nothing Nothing (Just jane) []
jane = Person2 "623" "Jane" "Fox" Female (Just ann) Nothing (Just john) []
janesTwin = Person2 "625" "JanesTwin" "Box" Female (Just ann) Nothing (Just john) []
ann  = Person2 "343" "Ann"  "Doe" Female Nothing Nothing Nothing [jane, janesTwin]

{-
  1.2.
  - Define a function
    parentCheck :: Person2 -> Bool
    that checks whether the given person is one of the children of its parents.
-}
parentCheck :: Person2 -> Bool
parentCheck p = elem (personId2 p) cm && elem (personId2 p) cf
    where
        cm = map personId2 $ maybe [] children2 $ mother2 p
        cf = map personId2 $ maybe [] children2 $ father2 p

{-
  1.3.
  - Define a function
    sister :: Person2 -> Maybe Person2
    that returns the sister of a person, if such exists.
-}

sister :: Person2 -> Maybe Person2
sister p
    | null sisters = Nothing
    | otherwise = Just (head sisters)
    where
        sisters = cm ++ cf
        cm = filter (\x -> sex2 x == Female && personId2 p /= personId2 x) $ maybe [] children2 $ mother2 p
        cf = filter (\x -> sex2 x == Female && personId2 p /= personId2 x) $ maybe [] children2 $ father2 p
--
--checkForChildren p = filter (\x -> sex2 x == Female && personId2 p /= personId2 x) $ maybe [] children2 $ p

{-
  1.4.
  - Define a function that returns all descendants of a person.
    descendant :: Person2 -> [Person2]
-}

descendant :: Person2 -> [Person2]
descendant p = dfs (children2 p) (children2 p)
    where
        dfs []      ys   = ys
        dfs (x:xs)  ys  = (dfs xs zs) ++ (dfs (children2 x) [])
            where zs = ys ++ (children2 x)

-- EXERCISE 02 =======================================================================
data MyList a = Empty | Cons a (MyList a) deriving (Show,Read,Ord,Eq)

infixr 5 -+-
(-+-) = Cons

{-
  2.1.
  - Define
    listHead :: MyList a -> Maybe a
-}

listHead :: MyList a -> Maybe a
listHead Empty = Nothing
listHead (Cons a _ ) = Just a

{-
  2.2.
  - Define a function that works like 'map' but works on a 'MyList' type:
    listMap :: (a -> b) -> MyList a -> MyList b
-}

listMap :: (a -> b) -> MyList a -> MyList b
listMap _ Empty = Empty
listMap f (Cons x xs) = (f x) -+- (listMap f xs)

-- EXERCISE 03 =======================================================================
data Tree a = Null | Node a (Tree a) (Tree a) deriving (Show,Eq)
{-
  3.1.
  - Define a function
    treeMax :: Ord a => Tree a -> a
    that finds the maximum element in a tree. Return an error if the tree is
    empty.
-}

intTree :: Tree Int
intTree = Node 10 (Node 2 Null Null) (Node 3 Null Null)

treeMax :: Ord a => Tree a -> a
treeMax Null = error "Tree is empty"
treeMax (Node x Null Null) = x
treeMax (Node x left Null) = max x (treeMax left)
treeMax (Node x Null right) = max x (treeMax right)
treeMax (Node x left right) = maximum [x, treeMax left, treeMax right]

{-
  3.2.
  - Define a function
    treeToList :: Ord a => Tree a -> [a]
    that will collect in a list all elements from inner nodes of a tree by doing
    an in-order (left-root-right) traversal.
-}

treeToList :: Ord a => Tree a -> [a]
treeToList Null = []
treeToList (Node n left right) = (treeToList left) ++ [n] ++ (treeToList right)

{-
  3.3.
  - Define a function to prune the tree at a given level (root has level 0).
    levelCut :: Int -> Tree a -> Tree a
-}

levelCut :: Int -> Tree a -> Tree a
levelCut level tree = levelCut2 tree level 0

levelCut2 :: (Eq a1, Num a1) => Tree a2 -> a1 -> a1 -> Tree a2
levelCut2 Null _ _ = Null
levelCut2 (Node n left right) level actualLevel 
 | level == actualLevel = (Node n Null Null) 
 | otherwise =  (Node n (levelCut2 left level (actualLevel+1)) (levelCut2 right level (actualLevel+1)))

-- EXERCISE 04 =======================================================================
{-
  4.1.
  - Define a function that converts a list into a sorted tree.
    listToTree :: Ord a => [a] -> Tree a
-}

listToTree :: Ord a => [a] -> Tree a
listToTree ls = buildTree (sort ls)

buildTree :: [a] -> Tree a
buildTree [] = Null
buildTree ls = (Node (ls !! half) (buildTree (Data.List.take (half) ls)) (buildTree (Data.List.drop (half+1) ls)))
 where half = quot (length ls) 2

{-
  4.2.
  - Using 'listToTree' and 'treeToList' defined previously, define these two 
    functions, define:
    sortAndNub :: Ord a => [a] -> [a]
-}

sortAndNub :: Ord a => [a] -> [a]
sortAndNub = treeToList . listToTree

-- EXERCISE 05 =======================================================================
{-
  5.1.
  - Define an 'Eq' instance for the 'Weekday' type that works like (==), except
    that two Fridays are never identical.
-}

data Weekday = 
  Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
  deriving (Show,Enum)
 
instance Eq Weekday where
  Monday    == Monday    = True
  Tuesday   == Tuesday   = True
  Wednesday == Wednesday = True
  Thursday  == Thursday  = True
  Friday    == Friday    = False
  Saturday  == Saturday  = True
  Sunday    == Sunday    = True
  _         == _         = False

{-
  5.2.
  - Define 'Person' as an instance of 'Show' type class so that instead of the
    values of partners and children only the respective person names are shown,
    which will enable the print out of an infinite structure of this type.
-}
ian = Person "111" "Ian" "Ianov" Male 65 (Just alice) [eve]
alice = Person "112" "Alice" "Aliinev" Female 30 Nothing []
eve = Person "113" "Eve" "evE" Female 29 Nothing []

data Person = Person
  { idNumber :: String
  , forename :: String
  , surname  :: String
  , sex      :: Sex
  , age      :: Int
  , partner  :: Maybe Person
  , children :: [Person]
  } deriving (Eq,Read,Ord)

instance Show Person where
    show (Person _ forename surname _ _ _ _) = show (forename ++ " " ++ surname)
{-LECTURE 11-} -- 

-- EXERCISE 01 =======================================================================

{- DON'T FORGET TO WRITE TYPE SIGNATURES-}

{-
  1.1.
  - Define a 'main' function that reads in two strings and prints them out
    concatenated and reversed.
-}

main :: IO ()
main = do
  a <- getLine
  b <- getLine
  putStrLn $ reverse $ a++b

{-
  1.2.
  - Write a function 'threeNumbers' that reads in three numbers and prints out
    their sum.
-}

threeNumbers :: IO ()
threeNumbers = do
  a <- getLine
  b <- getLine
  c <- getLine
  let aN = read a :: Integer    
  let bN = read b :: Integer 
  let cN = read c :: Integer     
  putStrLn $ show $ aN + bN + cN

-- EXERCISE 02 =======================================================================
{-
  2.1.
  - Define a function 'threeStrings' that reads in three strings and outputs them
    to the screen as one string, while it returns its total length.
    treeStrings :: IO Int
-}

treeStrings :: IO Int
treeStrings = do
  a <- getLine
  b <- getLine
  c <- getLine
  let res = a ++ b ++ c
  putStrLn res
  return $ length res

{-
  2.2.
  - Define a function 'askNumber9' that reads in a number and returns that number
    converted into an 'Int'. Input should be repeated until the user enters a
    number (a string containing only digits).
      askNumber9 :: IO Int
-}

fromMaybe :: Maybe a -> a
fromMaybe (Just a) = a

askNumber9 :: IO Int
askNumber9 = do
  putStrLn "Enter your number"
  n <- getLine
  let i = readMaybe n :: Maybe Int
  if i == Nothing then do
    putStr "Try harder! You can do this! Stay positive! "
    askNumber9 
  else    
    return $ fromMaybe i

{-
  2.3.
  - Define a function 'askUser m p' that returns an action that prints out 'm',
    reads in a string from the input, repeats the input until the input
    string satisfies the function 'p', and then returns the input string.
      askUser :: String -> (String -> Bool) -> IO String
  - Generalize this function to
      askUser' :: Read a => String -> (String -> Bool) -> IO a
-}

askUser :: String -> (String -> Bool) -> IO String
askUser m funcP = do
 putStrLn m
 a <- getLine
 if not (funcP a) then askUser m funcP
 else
   return "Excellent"

askUser' :: Read a => String -> (String -> Bool) -> IO a
askUser' = undefined

{-
  2.4.
  - Define a function that reads in strings until the user inputs an empty
    string, and then returns a list of strings received as input.
      inputStrings :: IO [String]
-}

inputStrings :: IO [String]
inputStrings = inputStrings' []
   
inputStrings' :: [String] -> IO [String]
inputStrings' acc = do
  a <- getLine
  if a == "" then return acc
  else
    inputStrings' (acc++[a])

-- EXERCISE 03 =======================================================================
{-
  3.1.
  - Define a function that reads in a number, then reads in that many
    strings, and finally prints these strings in reverse order.
-}
readStrings = do
  n <- getLine
  let nN = read n :: Integer
  res <- forM [1..nN] $ \x -> do 
          a <- getLine
          return a
  print $ reverse res
{-
  3.2.
  - Give recursive definitions for 'sequence' and 'sequence_'.
-}
sequence' :: (Foldable t, Monad m) => t (m a) -> m [a]
sequence' = Data.Foldable.foldr (liftM2 (:)) (return [])

sequence_' :: Monad m => [m a] -> m ()
sequence_' [] = return ()
sequence_' (m:ms) = do
    x <- m
    xs <- sequence_' ms
    return ()
{-
  3.3.
  - Give a recursive definitions for 'mapM' and 'mapM_'.
-}
mapM' :: Monad m => (t -> m a) -> [t] -> m [a]
mapM' _ [] = return []
mapM' f (x:xs) = do
  y <- f x
  ys <- mapM' f xs 
  return (y:ys)
  
mapM_' :: Monad m => (t -> m a) -> [t] -> m ()
mapM_' _ [] = return ()
mapM_' f (x:xs) = do
  y <- f x
  ys <- mapM_' f xs 
  return ()
  
{-
  3.4.
  - Define a function that prints out the Pythagorean triplets whose all sides
    are <=100. Every triplet should be in a separate line.
-}
pythTriplets = do
   forM [1..100] $ \xa -> do      
       forM [xa..100] $ \xb -> do
         forM [xb..100] $ \xc -> do
            if (isPythTriplet xa xb xc) then putStrLn $ "(" ++ show xa ++ ", " ++ show xb ++ ", " ++ show xc ++ ")"
            else putStr ""  
   putStr $ ""      

isPythTriplet a b c = 
          if (a^2 + b^2 == c^2)
          then True
          else False
-- EXERCISE 04 =======================================================================
{-
  4.1.
  - Define a function that removes from standard input every second line and
    prints the result to standard output.
      filterOdd :: IO ()
-}

filterOdd :: IO ()
filterOdd = do
  s <- getContents
  putStrLn $ unlines . map fst $ filter (\w -> even (snd w)) (zip (lines s) [0..])

{-
  4.2.
  - Define a function that prefixes each line from standard input with a line
    number (number + space).
      numberLines :: IO ()
-}

numberLines :: IO ()
numberLines = do
    s <- getContents
    putStr . unlines $ map f $ zip [1..] (lines s)
    where
        f (x, y) = (show x) ++ " " ++ y

{- 4.3.
  - Define a function to remove from standard input all words from a given set of
    words.
      filterWords :: Set String -> IO ()
-}

filterWords :: Set String -> IO ()
filterWords listWords = do
  s <- getContents
  putStrLn $ unlines $ filter (\w -> not (w `elem` listWords)) (lines s)

-- EXERCISE 05 =======================================================================  
{-
  5.1.
  - Define a function
    wc :: FilePath -> IO (Int, Int, Int)
    that counts the number of characters, words, and lines in a file.
-}

wc :: FilePath -> IO (Int, Int, Int)
wc f = do
   h <- openFile f ReadMode
   s <- hGetContents h
   let noChars = length s
   let noWords = length $ words s
   let noLines = length $ lines s
   return (noChars, noWords, noLines)

{-
  5.2. 
  - Define a function
    copyLines :: [Int] -> FilePath -> FilePath -> IO ()
    that copies given lines from the first file into the second.
-}

copyLines :: [Int] -> FilePath -> FilePath -> IO ()
copyLines ls f1 f2 = do
  h <- openFile f1 ReadMode
  s <- hGetContents h 
  let allLines = lines s
  let noL = (length ls) - 1
  toCopy <- forM [0..noL] $ \x -> do
             let a = allLines !! ( ls !! x) ++ "\n"             
             return a
  writeFile f2 $ concat toCopy

-- EXERCISE 06 =======================================================================
{-
  6.1.
  - Define a function
      wordTypes :: FilePath -> IO Int
    to compute the number of distinct words in the given file.
-}

wordTypes :: FilePath -> IO Int
wordTypes fs = do
  h <- openFile fs ReadMode
  s <- hGetContents h
  let distinctWords = nub $ words s
  return $ length distinctWords
