module Homework where
--
import Data.List
import Data.Char
import Data.Function ( fix )
--

-- Task 01

-- non accumulator style
factorial :: (Num a, Eq a) => a -> a
factorial = fix (\rec x -> if x == 0 then 1 else x * rec ( x -  1 ))

-- non accumulator style
sum' :: Num a => [a] -> a
sum' = fix (\rec (x:xs) -> if null xs then x else x + rec (xs) )

-- accumulator style
factorial' :: (Num a, Eq a) => a -> a
factorial' = fix (\rec acc x -> if x == 1 then acc else rec (x * acc) (x - 1)) 1

-- accumulator style
sum'' :: Num a => [a] -> a
sum'' = fix (\rec acc xs -> if null xs then acc else rec (acc + head xs) (tail xs)) 0

-- define list of natural numbers
nats :: [Integer]
nats = fix(\rec acc -> acc:(rec $ acc + 1)) 0

map' :: (a -> b) -> [a] -> [b]
map' f = fix(\rec xs -> if null xs then [] else (f $ head xs):(rec $ tail xs))

zip' :: [a] -> [b] -> [(a, b)]
zip' = fix(\rec xs ys -> if or[null xs, null ys] then [] else (head xs, head ys):(rec (tail xs) (tail ys) ))

-- Task 02
{-
Write a function:
subsets :: Int -> [a] -> [[a]]
which generates a list of all k-sized subsets of the given set.
Some examples:
subsets 0 [1,2,3]       -> [[]]
subsets 1 [1,2,3]       -> [[1],[2],[3]]
subsets 1 [1,2,2,3]     -> [[1],[2],[3]]
subsets 2 [1,2,3]       -> [[1,2],[1,3],[2,3]]
subsets 2 [1,2,2,3,3]   -> [[1,2],[1,3],[2,3]]
subsets 3 [1,2,3]       -> [[1,2,3]]
subsets 100 [1,2,3]     -> []
-}
{-
Funciton sub gets set of elements and divides it to the single elements.
After that, it merges single elements to the sets with certain number of
elements described as 'n'.
-}
sub :: Int -> [a] -> [[a]]
sub 0 _ = [[]]
sub _ [] = []
sub n (x:xs) = (map (x:) (sub (n-1) xs)) ++ sub n xs

subsets :: (Eq a) => Int -> [a] -> [[a]]
subsets n = sub n . nub

{-
partitions []       -> error
partitions [1]      -> [[[1]]]
partitions [1,2]    -> [[[1,2]],[[1],[2]]]
partitions [1,2,3]  -> [[[1,2,3]],[[1],[2,3]],[[1,2],[3]],[[2],[1,3]],[[1],[2],[3]]]
-}
{-
Beginning from one element of list (for egzample [1]) we start to merge
further elements. For instance in list [1,2] we will start disasembling 
list to parts [1], [2] and merge them: [[1,2]], [[2], [1]].
-}
makePartitions :: a -> [[a]] -> [[[a]]]
makePartitions x  []      = [[[x]]]
makePartitions x (xs:xss) = ((x:xs):xss) : map (xs:) (makePartitions x xss)

partitions :: [a] -> [[[a]]]
partitions  []    = [[]]
partitions (x:xs) = [ys | yss <- partitions xs, ys <- makePartitions x yss]