module Homework where
--
import Control.Monad.Reader
import Data.Char
import Data.List
import Data.Time.Clock ( UTCTime(..) )
import Data.Time.Calendar ( Day, gregorianMonthLength, fromGregorian, addDays, toModifiedJulianDay )
import Data.Time.Format ( formatTime, defaultTimeLocale )
import Text.Read

--

-- Task 01
{-
Define a recursive data type Pred that represents a boolean expression.
Use the type constructors And, Or and Not to represent boolean operations and the
Val constructor to represent a boolean value. Implement the eval function that
takes a Pred and returns its evaluated Bool value.
-}
data Pred = And Pred Pred | Or Pred Pred | Not Pred | Val Bool deriving (Show)

eval :: Pred -> Bool
eval (Val x)   = x
eval (And x y) = eval x && eval y
eval (Or x y)  = eval x || eval y
eval (Not x)   = not $ eval x

-- Task 02
{-
In this exercise, you will take a description of a meetup date, and return
the actual meetup date. Typically meetups happen on the same day of the week.
Examples of general descriptions are:
- the first Monday of January 2017
- the third Tuesday of January 2017
- the Wednesteenth of January 2017
- the last Thursday of January 2017
Note that \Monteenth", \Tuesteenth", etc are all made up words. There was a
meetup whose members realized that there are exactly 7 numbered days in a month
that end in \-teenth". Therefore, one is guaranteed that each day of the week
(Monday, Tuesday, ...) will have exactly one date that is named with \-teenth" in
every month.
Given examples of a meetup dates, each containing a month, day, year, and descriptor
(first, second, teenth, etc), calculate the date of the actual meetup. For example, if
given \First Monday of January 2017", the correct meetup date is 2017/1/2.
Write the following function:
dateFromDescription :: String -> Day
Where Day is a data type from the time package.
You may want to take a look at time package documentation (Calendar module in
particular) on Hackage. It should come by default with Haskell platform, but in case
you don't have it, you can install it with stack or cabal depending on what you use.
If you are running your scripts with ghci than you should run cabal install time,
and if you are using stack repl than you can install it with stack install time.
-}
data Schedule = First
              | Second
              | Third
              | Fourth
              | Last
              | Teenth
              deriving (Enum)

data Weekday = Monday
             | Tuesday
             | Wednesday
             | Thursday
             | Friday
             | Saturday
             | Sunday
             deriving (Enum)

teenthList = ["monteenth", "tuesteenth", "wednesteenth", "thursteenth", "friteenth", "saturteenth", "sunteenth"]
scheduleList = ["first","second","third","fourth","last","teenth"]
weekdayList = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
monthList = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"]

toSchedule :: [String] -> Schedule    
toSchedule [x] 
    | x `elem` teenthList = Teenth
    | otherwise = case x of 
        "first"  -> First
        "second" -> Second
        "third"  -> Third
        "fourth" -> Fourth
        "last"   -> Last

monthToInt :: Num p => [String] -> p        
monthToInt [x] = case x of
    "january"   -> 1
    "february"  -> 2
    "march"     -> 3
    "april"     -> 4
    "may"       -> 5
    "june"      -> 6
    "july"      -> 7
    "august"    -> 8
    "september" -> 9
    "october"   -> 10
    "november"  -> 11
    "december"  -> 12

dayToWeekday :: [String] -> Weekday    
dayToWeekday [x] = case x of 
    "monday"       -> Monday
    "monteenth"    -> Monday
    "tuesday"      -> Tuesday
    "tuesteenth"   -> Tuesday
    "wednesday"    -> Wednesday
    "wednesteenth" -> Wednesday
    "thursday"     -> Thursday
    "thursteenth"  -> Thursday
    "friday"       -> Friday
    "friteenth"    -> Friday
    "saturday"     -> Saturday
    "saturteenth"  -> Saturday
    "sunday"       -> Sunday               
    "sunteenth"    -> Sunday

toDate :: Schedule -> Weekday -> Integer -> Int -> Day
toDate schedule weekday year month =
    let fromGregorian' = fromGregorian year month
        makeOffset' day = toInteger $ makeOffset (toWeekday day) weekday
    in  case schedule of
        Teenth -> addDays (makeOffset' day) day 
          where day = fromGregorian' 13
        Last -> addDays (makeOffset' day) day 
          where day = addDays (-6) (fromGregorian' 31)
        _ -> addDays days day 
          where day = fromGregorian' 1
                days = makeOffset' day + 7 * toInteger (fromEnum schedule)

makeOffset :: Weekday -> Weekday -> Int
makeOffset a b = delta `mod` 7 
   where delta = fromEnum b - fromEnum a

toWeekday :: Day -> Weekday
toWeekday day = toEnum $ fromInteger $ (toModifiedJulianDay day + 2) `rem` 7

dateFromDescription :: String -> Day 
dateFromDescription s = toDate schedule weekday year month
  where year       = head $ takeYear $ words s
        month      = monthToInt $ filter (`elem` monthList) lowerWords
        weekday    = dayToWeekday $ filter (`elem` (weekdayList ++ teenthList)) lowerWords 
        schedule   = toSchedule $ filter (`elem` (scheduleList ++ teenthList)) lowerWords        
        lowerWords = words $ map toLower s         

takeYear :: [String] -> [Integer]
takeYear []     = []
takeYear (x:xs) = case readMaybe x :: Maybe Integer of 
    Nothing -> takeYear xs
    Just x  -> x : takeYear xs

-- Task 03

testTree = Node 1 (Node 2 Leaf Leaf) (Node 3 (Node 4 Leaf Leaf) Leaf)

data Tree a
  = Leaf | Node a (Tree a) (Tree a)
  deriving (Eq, Show)

-- a)
{-
Define a function treeFilter that takes a predicate and a Tree
and removes those subtrees that do not satisfy the given predicate (with any
children).
-}
treeFilter :: (a -> Bool) -> Tree a -> Tree a
treeFilter p Leaf = Leaf
treeFilter p (Node a left right)
 | p a = (Node a (treeFilter p left) (treeFilter p right))
 | otherwise = Leaf

-- b)
{-
Define a function levelMap that takes some binary function and
applies it to the tree. The function that is being applied ta
-}
levelMap :: (Int -> a -> b) -> Tree a -> Tree b
levelMap p t = levelMap' p t 0

levelMap' :: Num a1 => (a1 -> t -> a2) -> Tree t -> a1 -> Tree a2
levelMap' p Leaf _ = Leaf
levelMap' p (Node a left right) x = (Node (p x a) 
                                    (levelMap' p left (x+1))
                                    (levelMap' p right (x+1)))

-- c)
{-
Define a function isSubtree that takes two instances of Tree and
checks whether the first tree appears as part of the second.
-}
isSubtree :: Eq a => Tree a -> Tree a -> Bool
isSubtree Leaf _           = True
isSubtree subTree testTree = isSubtree' subTree testTree 

isSubtree' :: Eq a => Tree a -> Tree a -> Bool
isSubtree' Leaf Leaf = True
isSubtree' Leaf _    = False
isSubtree' _ Leaf    = False
isSubtree' subtree@(Node s l1 l2) (Node t y1 y2)
 | s == t = and[isSubtree' l1 y1, isSubtree' l2 y2]
 | otherwise = or[isSubtree' subtree y1, isSubtree' subtree y2]
