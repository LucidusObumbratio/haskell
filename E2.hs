{-# LANGUAGE NoMonomorphismRestriction #-}
--
module Exercises where
--
import Data.List
import Data.Char
--

{-LECTURE 04-} 

-- EXERCISE 01 =======================================================================

-- Define 'headHunter xss' that takes the head of the first list element. If 
-- the first element has no head, it takes the head of the second element.
-- If the second element has no head, it takes the head of the third element.
-- If none of this works, the function returns an error.
ex411 = headHunter
headHunter :: [[a]] -> a
headHunter ((h:_):_) = h
headHunter (_:(h:_):_) = h
headHunter (_:_:(h:_):_) = h
headHunter _ = error "My Super Private Error"

-- Define 'firstColumn m' that returns the first column of a matrix.
-- firstColumn [[1,2],[3,4]] => [1,3]
-- Check what happens if the input is not a valid matrix.
ex412 = firstColumn
firstColumn :: [[a]] -> [a]
firstColumn l = [x | (x:_) <- l]

-- Define 'shoutOutLoud' that repeats three times the initial letter of each
-- word in a string.
-- shoutOutLoud :: String -> String
-- shoutOutLoud "Is anybody here?" => "IIIs aaanybody hhhere?"
ex413 = shoutOutLoud
shoutOutLoud :: String -> String
shoutOutLoud str = concat [x : x : row ++ " " | row@(x:_) <- (words str)]

-- EXERCISE 02 =======================================================================

-- Define 'pad' that pads the shorter of two the strings with trailing spaces 
-- and returns both strings capitalized.
-- pad :: String -> String -> (String, String)
-- pad "elephant" "cat" => ("Elephant", "Cat     ")
ex421 = pad
pad :: String -> String -> (String, String)
pad str1@(x:xs) str2@(y:ys) = ((toUpper x):xs ++ spaces(l2 - l1),(toUpper y):ys ++ spaces(l1 - l2))
 where l1 = length str1
       l2 = length str2
       spaces n
        | n > 0     = concat $ take n $ repeat " "
        | otherwise = ""

-- Define 'quartiles xs' that returns the quartiles (q1,q2,q3) of a given list.
-- The quartiles are elements at the first, second, and third quarter of a list
-- sorted in ascending order. (You can use the built-int 'splitAt' function and
-- the previously defined 'median' function.)
-- quartiles :: [Int] -> (Double,Double,Double)
-- quartiles [3,1,2,4,5,6,8,0,7] => (1.5, 4.0, 6.5)
ex422 = quartiles
quartiles :: [Int] -> (Double,Double,Double)
quartiles ls
 | odd len   = (median $ take half sortedls, median sortedls, median $ drop (half + 1) sortedls)
 | otherwise = (median $ take half sortedls, median sortedls, median $ drop half sortedls)
 where sortedls = sort ls
       len      = length ls
       half     = len `div` 2

median :: (Integral a, Fractional b) => [a] -> b
median [] = error "median: Empty list"
median xs 
  | odd l     = realToFrac $ ys !! h
  | otherwise = realToFrac (ys !! h + ys !! (h-1)) / 2
  where l  = length xs
        h  = l `div` 2
        ys = sort xs

-- EXERCISE 03 =======================================================================

-- Redo Exercise 2 using 'let' instead of 'where'.
ex431 = pad'
pad' :: String -> String -> (String, String)
pad' str1@(x:xs) str2@(y:ys) = 
 let l1 = length str1
     l2 = length str2
     spaces n
      | n > 0     = concat $ take n $ repeat " "
      | otherwise = ""
 in ((toUpper x):xs ++ spaces(l2 - l1),(toUpper y):ys ++ spaces(l1 - l2))


ex432 = quartiles'
quartiles' :: [Int] -> (Double,Double,Double)
quartiles' ls =
 let
 sortedls = sort ls
 len      = length ls
 half     = len `div` 2
 in case () of
 _| (odd len)   -> (median $ take half sortedls, median sortedls, median $ drop (half + 1) sortedls)
 _| (even len)   -> (median $ take half sortedls, median sortedls, median $ drop half sortedls)

-- EXERCISE 04 =======================================================================

-- Write a function that takes in a pair (a,b) and a list [c] and returns the
-- following string:
-- "The pair [contains two ones|contains one one|does not contain a single one]
-- and the second element of the list is <x>"
ex441 = foo
foo :: Show c => (Int, Int) -> [c] -> String
foo p (_:z:_) = "The pair " ++ (case p of
 (1,1) -> "contains two ones"
 (1,_) -> "contains one one"
 (_,1) -> "contains one one"
 (_,_) -> "does not contain a single one") ++ 
 " and the second element of the list is " ++ show z

{-LECTURE 05-}

-- EXERCISE 01 =======================================================================

-- Define a recursive function to compute the product of a list of elements.
-- product' :: Num a => [a] -> a
ex511 = product'
product' :: Num a => [a] -> a
product' [] = 1
product' (x:xs) = x * product' xs

-- Define a recursive function 'headsOf' that takes a list of lists and
-- returns a list of their heads.
-- headsOf :: [[a]] -> [a]
-- headsOf [[1,2,3],[4,5],[6]] => [1,4,6]
ex512 = headsOf
headsOf :: [[a]] -> [a]
headsOf [] = []
headsOf ([]:xss) = headsOf xss
headsOf ((x:_):xs) = x : headsOf xs

-- EXERCISE 02 =======================================================================

-- Define a recursive function 'modMult n m xs' that multiplies each element of
-- a list 'xs' with 'n' modulo 'm'.
ex521 = modMult
modMult _ 0 _ = error "Error"
modMult _ _ [] = []
modMult n m (x:xs) = ((n * x) `mod` m) : modMult n m xs

-- Define a function 'addPredecessor' that adds to each element of a list the
-- value of the preceding element. The first element gets no value added.
-- addPredecessor :: Num a => [a] -> [a]
-- addPredecessor [3,2,1] => [3,5,3]
ex522 = addPredecessor
addPredecessor :: Num a => [a] -> [a]
addPredecessor [] = []
addPredecessor (x:xs) = x : addPredecessor' x xs
 where addPredecessor' _ [] = []
       addPredecessor' n (x:xs) = n + x : addPredecessor' x xs

-- EXERCISE 03 =======================================================================

-- Define 'equalTriplets' that filters from a list of triplets (x,y,z) all
-- triplets for which x==y==z.
-- equalTriplets [(1,2,3),(2,2,2),(4,5,6)] => [(2,2,2)]
ex531 = equalTriplets
equalTriplets [] = []
equalTriplets ((x,y,z):xs)
 | and[x==y, y==z] = (x,x,x) : equalTriplets xs
 | otherwise = equalTriplets xs

-- Define your own version of the replicate function:
-- replicate' :: Int -> a -> [a]
ex532 = replicate'
replicate' 0 x = []
replicate' n x = x : (replicate' (n-1) x)

-- EXERCISE 04 =======================================================================

-- Define your own recursive version of the drop function:
-- drop' :: Int -> [a] -> [a].
-- Define drop'' (a wrapper function) so that for n < 0 the function drops
-- the elements from the end of the list. You can use 'reverse'.
ex541 = drop'
drop' :: Int -> [a] -> [a]
drop' _ [] = []
drop' 0 xs = xs
drop' n (x:xs) = drop' (n-1) xs

ex541' = drop''
drop'' :: Int -> [a] -> [a]
drop'' n xs
 | n < 0 = reverse (drop' (-n) (reverse xs))
 | otherwise = drop' n xs

-- Define a recursive function 'takeFromTo n1 n2 xs'.
-- takeFromTo :: Int -> Int -> [a] -> [a]
ex542 = takeFromTo
takeFromTo :: Int -> Int -> [a] -> [a]
takeFromTo 0 0 _ = []
takeFromTo _ _ [] = []
takeFromTo 0 xr (x:xs) = x : takeFromTo 0 (xr-1) xs
takeFromTo xl xr (x:xs)
 | xl <= xr = takeFromTo (xl-1) (xr-1) xs
 | otherwise = []
 
-- EXERCISE 05 =======================================================================

-- Define a recursive function 'eachThird' that retains every third element
-- in a list.
-- eachThird :: [a] -> [a]
-- eachThird "zagreb" => "gb"
ex551 = eachThird
eachThird [] = []
eachThird (_:[]) = []
eachThird (_:_:[]) = []
eachThird (x:y:z:xs) = z : eachThird xs

-- Define a recursive function 'crossZip' that zips two lists in a "crossing"
-- manner:
-- crossZip [1,2,3,4,5] [4,5,6,7,8] => [(1,5),(2,4),(3,7),(4,6)]
ex552 = crossZip
crossZip [] _ = []
crossZip _ [] = []
crossZip (_:[]) (_:[]) = []
crossZip (x:[]) (_:y2:_) = [(x,y2)]
crossZip (_:x2:_) (y:[]) = [(x2,y)]
crossZip (x1:x2:xs) (y1:y2:ys) = (x1,y2) : (x2,y1) : crossZip xs ys

-- EXERCISE 06 =======================================================================

-- Write an accumulator-style recursive definition of
-- length' :: [a] -> Int

ex561 = length'
length' :: [a] -> Int
length' xs = length'' xs 0
 where length'' []     n = n
       length'' (_:xs) n = length'' xs (n+1)

-- Write an accumulator-style recursive definition of
--     maxUnzip :: [(Int, Int)] -> (Int, Int)
-- that returns the maximum element at the first position and the maximum
-- element at the second position in a pair, i.e., it's equivalent to:
--     maxUnzip zs = (maximum xs, maximum ys)
--         where (xs,ys) = unzip zs
-- If the list is empty, return an "empty list" error.
-- Now write a standard recursive definition maxUnzip' (without an accumulator).
ex562 = maxUnzip
maxUnzip :: [(Int, Int)] -> (Int, Int)
maxUnzip [] = error "Error"
maxUnzip (x:ls) = maxUnzip' ls x
 where maxUnzip' []               x = x
       maxUnzip' ((x,y):ls) (mx,my) = maxUnzip' ls ((max x mx),(max y my))

ex562' = maxUnzip'
maxUnzip' :: [(Int, Int)] -> (Int, Int)
maxUnzip' [] = error "Error"
maxUnzip' [x] = x
maxUnzip' ((x,y):ls) = (max x mx, max y my)
 where (mx,my) = maxUnzip' ls