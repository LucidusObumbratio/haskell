module Homework where
--
import Data.List
import Data.Char
--

-- Task 01
{-Given a DNA strand, return its RNA complement (per RNA transcrip-
tion).
Both DNA and RNA strands are a sequence of nucleotides.
The four nucleotides found in DNA are adenine (A), cytosine (C), guanine (G) and
thymine (T).
The four nucleotides found in RNA are adenine (A), cytosine (C), guanine (G) and
uracil (U).
Given a DNA strand, its transcribed RNA strand is formed by replacing each nucle-
otide with its complement:
* G -> C
* C -> G
* T -> A
* A -> U
Make sure you throw an error if the nucleotide doesn't exist.-}
changeChar :: Char -> Char
changeChar c
 | c == 'A' = 'U'
 | c == 'T' = 'A'
 | c == 'C' = 'G'
 | c == 'G' = 'C'
 | otherwise = error "No such nucleotide"

toRNA :: String -> String
toRNA [] = []
toRNA (x:xs) = changeChar x : toRNA xs

-- Task 02
{-Extract the scrabble scores from a legacy system and convert them to the
new system.
The old system stored a list of letters per score:
[(Int, String)]
Example rendering of old data structure (you don't have to implement this):
* 1 point: "A", "E", "I", "O", "U", "L", "N", "R", "S", "T",
* 2 points: "D", "G",
* 3 points: "B", "C", "M", "P",
* ...
The shiny new scrabble system instead stores the score per letter, which makes it
much faster and easier to calculate the score for a word. It also stores the letters in
lower-case regardless of the case of the input letters:
[(Char, Int)]
* "a" is worth 1 point.
* "b" is worth 3 points.
* "c" is worth 3 points.
* ...
Define a function which transforms data from the old system to the new one :
transform :: [(Int, String)] -> [(Char, Int)]
Define a function that renders data from the new system
render :: [(Char, Int)] -> String
in the following format :
"a" is worth 1 point.
"b" is worth 3 points.
"c" is worth 3 points.
Use backslash nto escape quotes and add newlines e.g.: n", nn-}
transform :: [(Int,String)] -> [(Char,Int)]
transform [] = []
transform ((_,[]):ys) = transform ys
transform ((n,x:xs):ys) = (toLower x, n) : transform((n,xs):ys)

render :: [(Char,Int)] -> String
render [] = ""
render ((c,1):xs) = '\"' : c : '\"' : " is worth 1 point.\n" ++ render xs
render ((c,n):xs) = '\"' : c : '\"' : " is worth " ++ show n ++ " points.\n" ++ render xs

-- Task 03
{-Your function should output :
numberToWords 109324 ) "one hundred nine thousand three hundred twenty-four"
Also, you only need to implement conversion for numbers up to millions.
Here are some more examples :
numberToWords 1 ) "one"
numberToWords 10 ) "ten"
numberToWords 11 ) "eleven"
numberToWords 100 ) "one hundred"
numberToWords 115 ) "one hundred fifteen"
numberToWords 1213 ) "one thousand two hundred thirteen"
numberToWords 1005 ) "one thousand five"
numberToWords 22213 ) "twenty-two thousand two hundred thirteen"
...
numberToWords 1000000 ) "one million"
numberToWords 1000001 ) "one million one"
numberToWords 1002001 ) "one million two thousand one"-}
strNumToWord numWord 
 | numWord == '0' = ""
 | numWord == '1' = "one "
 | numWord == '2' = "two "
 | numWord == '3' = "three "
 | numWord == '4' = "four "
 | numWord == '5' = "five "
 | numWord == '6' = "six "
 | numWord == '7' = "seven "
 | numWord == '8' = "eight "
 | numWord == '9' = "nine "
 | otherwise = error ("nan" ++ show numWord)
 
specialTeen numWord
 | numWord == '0' = " ten "
 | numWord == '1' = " eleven "
 | numWord == '2' = " twelve "
 | numWord == '3' = " thirteen "
 | numWord == '4' = " fourteen "
 | numWord == '5' = " fifteen "
 | numWord == '6' = " sixteen "
 | numWord == '7' = " seventeen "
 | numWord == '8' = " eighteen "
 | numWord == '9' = " nineteen "
 | otherwise = error "nan"
 
twoDecimalNum (x1:x2:[])
 | x1 == '0' = strNumToWord x2
 | x1 == '1' = specialTeen x2
 | x1 == '2' = " twenty" ++ specialTens x2
 | x1 == '3' = " thirty" ++ specialTens x2
 | x1 == '4' = " fourty" ++ specialTens x2
 | x1 == '5' = " fifty"  ++ specialTens x2
 | x1 == '6' = " sixty"  ++ specialTens x2
 | x1 == '7' = " seventy"++ specialTens x2
 | x1 == '8' = " eighty" ++ specialTens x2
 | x1 == '9' = " ninety" ++ specialTens x2
 | otherwise = error "nan"

specialTens c
| c == '0' = " "
| otherwise = '-' : strNumToWord c
 
bigNumber [] = ""
bigNumber [x] = strNumToWord x
bigNumber str@(x:xs)
 | x == '0' = bigNumber xs
 | sl > 6 = bigNumber (take (sl - 6) str) ++ " million " ++ bigNumber (drop (sl - 6) str)
 | sl > 3 = bigNumber (take (sl - 3) str) ++ " thousand " ++ bigNumber (drop (sl - 3) str)
 | sl == 3 = strNumToWord x ++ " hounderd " ++ bigNumber xs
 | sl == 2 = twoDecimalNum str
 where sl = length str
 
numberToWords :: Int -> String
numberToWords n = bigNumber $ show n

-- Task 04
undefined' :: a
undefined' = error "Undefined"












