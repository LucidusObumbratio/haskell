{-# LANGUAGE NoMonomorphismRestriction #-}
--
module Exercises where
--
import Data.List
import Data.Char
--

{-LECTURE 06-}

-- EXERCISE 01 =======================================================================

{-
  1.1.
  - Write an accumulator-style recursive definition of
    length' :: [a] -> Int
-}

ex611 = length'
length' xs = length'' xs 0
 where length'' []     n = n
       length'' (_:xs) n = length'' xs (n+1)

{-
  1.2
  - Write an accumulator-style recursive definition of
      maxUnzip :: [(Int, Int)] -> (Int, Int)
    that returns the maximum element at the first position and the maximum
    element at the second position in a pair, i.e., it's equivalent to:
      maxUnzip zs = (maximum xs, maximum ys)
        where (xs,ys) = unzip zs
    If the list is empty, return an "empty list" error.

  - Now write a standard recursive definition (without an accumulator).
-}
ex612 = maxUnzip
maxUnzip :: [(Int, Int)] -> (Int, Int)
maxUnzip [] = error "Error"
maxUnzip (x:ls) = maxUnzip' ls x
 where maxUnzip' []               x = x
       maxUnzip' ((x,y):ls) (mx,my) = maxUnzip' ls ((max x mx),(max y my))

ex662' = maxUnzip'
maxUnzip' :: [(Int, Int)] -> (Int, Int)
maxUnzip' [] = error "Error"
maxUnzip' [x] = x
maxUnzip' ((x,y):ls) = (max x mx, max y my)
 where (mx,my) = maxUnzip' ls
