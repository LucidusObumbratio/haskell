module Homework where
--
import Data.List hiding (insert, lookup)
import Data.Char
import Prelude hiding (insert, lookup)
--

-- Task 01
{-Function that checks if a year is a leap year. In Gregorian calendar
leap years occur :
* on every year that is divisible by 4
* except every year that is evenly divisible by 100
* unless the year is also evenly divisible by 400
For example, 1997 is not a leap year, but 1996 is and 1900 is not leap year, but 2000
is.-}
isLeapYear :: Int -> Bool
isLeapYear year = or[(year `mod` 400) == 0,and[(year `mod` 4) == 0, (year `mod` 100) /= 0]]

leapList :: [Int]
leapList = [ x | x <- [1996..2017], isLeapYear x]

-- Task 02
{- Implement the exponential function.
(a) For that you will have to first write a function which evaluates polynomial of
single indeterminante x where polynomial coeffcients are represented by a list
of Doubles. The polynomial function can be expressed as sum ( a_k * x^k )
evaluate :: Double -> [Double] -> Double

(b) After that is done, define a function that calculates the nth factorial :
factorial :: Double -> Double

(c) Next, define an infinite list of coeffcients for Maclaurin series.
maclaurin :: [Double]

(d) And finally, combine evaluate, factorial and maclaurin to create a function
which approximates ex from the first 170 terms of the series.
exp' :: Double -> Double-}
evaluate :: Double -> [Double] -> Double
evaluate x al = sum[ a * x**num | (a,num) <- zip al [0..]]

factorial :: Double -> Double
factorial n = product[2..n]

maclaurin :: [Double]
maclaurin = [ 1/(factorial x) | x <- [0..]]

exp' :: Double -> Double
exp' z = evaluate z (take 170 maclaurin)

-- Task 03
{-Implement some polymorphic utility functions
for working on lists of key value pairs where keys are strings.

(a) The Function for getting a pair with a certain key which should return a list
with a single element as a result (or no elements if key doesn't exist) :
findItem :: [(String, a)] -> String -> [(String, a)]

(b) Function that checks if a list contains an element with a certain key :
contains :: [(String, a)] -> String -> Bool

(c) Function that tries to retrieve a value with a certain key or throws an error if
the key doesn't exist (example of error function usage : error "I'm an error
message") :
lookup :: [(String, a)] -> String -> a

(d) Function that inserts a new key value pair. If key already exists than do nothing:
insert :: [(String, a)] -> (String, a) -> [(String, a)]

(e) Function that removes a key value pair with the certain key :
remove :: [(String, a)] -> String -> [(String, a)]

(f) Function that updates the value of a certain key (if the key doesn't exist,
function does nothing):
update :: [(String, a)] -> String -> a -> [(String, a)]-}

findItem :: [(String, a)] -> String -> [(String, a)]
findItem myMap key = [ mapEntitiy | mapEntitiy <- myMap, fst mapEntitiy == key]

contains :: [(String, a)] -> String -> Bool
contains myMap key = not (null (findItem myMap key))

lookup :: [(String, a)] -> String -> a
lookup myMap key
 | length mapEntitiy > 0 = snd (mapEntitiy !! 0)
 | otherwise = error "Cannot find given key"
 where mapEntitiy = findItem myMap key

insert :: [(String, a)] -> (String, a) -> [(String, a)]
insert myMap newElem
 | not(contains myMap (fst newElem)) = newElem:myMap
 | otherwise = myMap

remove :: [(String, a)] -> String -> [(String, a)]
remove myMap key = [ mapEntitiy | mapEntitiy <- myMap, key /= (fst mapEntitiy)]

update :: [(String, a)] -> String -> a -> [(String, a)]
update myMap key value
 | contains myMap key = insert (remove myMap key) (key,value)
 | otherwise = myMap

-- Task 04
{-Implement a function that is calculating
how similar two texts are.
To do that, we can imagine that each text is a vector in vector space composed of
union of words from those two texts.
Their similarity can than be expressed as cosine of angle between those two vectors,
with values ranging from 0 (meaning not similar) to 1 (meaning exactly the same /
angle is 0). Values can't be negative because we are relying on word frequency in
this case. This is also known as cosine similarity so check the wiki in case you need
some help.
Make sure you remove all non letter characters from text (space is considered non
letter character so be careful with that) and convert it to lowercase to get best
results.

Implement function :
cosineSimilarity :: String -> String -> Double

Which takes in two texts and returns a value between 0 and 1 that describes how
"similar" they are.
For strings
"Haskell makes me giddy! I want to jump around like a little girl."
and
"Haskell makes me happy! So much that I want to jump around like a little
pony."
cosineSimilarity should give you 0.76271276980969-}

onlyWords :: String -> String
onlyWords str = [ toLower c | c <- str, or[and[ord (toLower c) > 96, ord (toLower c) < 123], c == ' ']]

stringToWords :: String -> [String]
stringToWords str = words (onlyWords str)

intersectCount :: String -> String -> Int
intersectCount str1 str2 = length ( intersect (stringToWords str1) (stringToWords str2))

getIntersection :: String -> String -> [String]
getIntersection str1 str2 = intersect (stringToWords str1) (stringToWords str2)

countOccurances :: String -> String -> Int
countOccurances str value = sum[ 1 | word <- stringToWords str, value == word]

computeNumerator :: String -> String -> Int
computeNumerator str1 str2 = sum[ (countOccurances str1 commonWord) * (countOccurances str2 commonWord) | commonWord <- getIntersection str1 str2 ]

computeDenominator :: String -> String -> Double
computeDenominator str1 str2 = sqrt(fromIntegral(sum[(countOccurances str1 word)^2 | word <- stringToWords str1])) * sqrt(fromIntegral(sum[(countOccurances str2 word)^2 | word <- stringToWords str2]))

cosineSimilarity :: String -> String -> Double
cosineSimilarity str1 str2
 | denominator == 0 = 0.0
 | otherwise = fromIntegral (numerator) / denominator 
 where numerator = computeNumerator str1 str2
       denominator = computeDenominator str1 str2
















