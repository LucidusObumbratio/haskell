{-# LANGUAGE NoMonomorphismRestriction #-}
--

module Exercises where
import Data.List
import Data.Char
import Data.Ord (comparing)
--
{-LECTURE 08-} --
-- EXERCISE 01 =======================================================================
{-
  Define the following functions using composition and pointfree style (you may
  of course use local definitions):
-}

{-
  1.1.
  - Define 'sumEven' that adds up elements occurring at even (incl. zero) 
    positions in a list.
    sumEven :: [Integer] -> Integer
    sumEven [1..10] => 25
-}

sumEven :: [Integer] -> Integer
sumEven = sum . map(\(x,y) -> y) . filter(even . fst) . zip [0..]

{-
  1.2.
  - Define 'filterWords ws s' that removes from string 's' all words contained
    in the list 'ws'.
    filterWords :: [String] -> String -> String
-}

filterWords :: [String] -> String -> String
filterWords ws = unwords . filter( `notElem` ws) . words

{-
  1.3.
  - Define 'initials3 d p s' that takes a string 's' and turns it into a string
    of initials. The function is similar to 'initials2' but additionally delimits
    the initials with string 'd' and discards the initials of words that don't
    satisfy the predicate 'p'.
    initials3 :: String -> (String -> Bool) -> String -> String
    initials3 "." (/="that") "a company that makes everything" => "A.C.M.E."
  - Use this function to define the 'initials' function.
-}

initials3 :: String -> (String -> Bool) -> String -> String
initials3 d p = concat . map(\x -> x:d) . map(toUpper . head) . filter(p) . words

initials = initials3 "" (/="")
-- EXERCISE 02 =======================================================================
{-
  Just a reminder that EVERY function in this file needs to have a type signature ;)
-}

{-
  2.1.
  - Define 'maxDiff xs' that returns the maximum difference between consecutive
    elements in the list 'xs'.
    maxDiff :: [Int] -> Int
    maxDiff [1,2,3,5,1] => 4
  - Define 'maxMinDiff' that returns the pair (min_difference, max_difference).
-}

maxDiff :: [Int] -> Int
maxDiff [] = error "Error"
maxDiff (_:[]) = 0
maxDiff xs@(x:y:_) = maxDiff' (abs (x-y)) xs

maxDiff' acc [] = acc
maxDiff' acc (_:[]) = acc
maxDiff' acc (x:y:xs) = maxDiff' (max acc (abs $ (x - y))) (y:xs)

maxMinDiff [] = error "Error"
maxMinDiff (_:[]) = (0, 0)
maxMinDiff xs@(x:y:_) = maxMinDiff' (diff,diff) xs
 where diff = abs $ (x - y)

maxMinDiff' acc [] = acc
maxMinDiff' acc (_:[]) = acc
maxMinDiff' (accX, accY) (x:y:xs) = maxMinDiff' ((min accX diff), (max accY diff)) (y:xs) 
 where diff = abs $ (x - y)

{-
  2.2.
  - Define 'studentsPassed' that takes as input a list [(NameSurname,Score)] and
    returns the names of all students who scored at least 50% of the maximum 
    score.
-}

studentsPassed xs = map(fst) $ filter(\(x,y) -> y >=(0.5 * maxScore)) xs
 where maxScore = snd $ maximumBy (comparing snd) xs

-- EXERCISE 03 =======================================================================
{-
  3.1.
  - Define 'isTitleCased' that checks whether every word in a string is
    capitalized.
    isTitleCased :: String -> Bool
    isTitleCased "University Of Zagreb" => True
-}

isTitleCased :: String -> Bool
isTitleCased = all (isUpper . head) . words

{-
  3.2.
  - Define 'sortPairs' that sorts the list of pairs in ascending order with
    respect to the second element of a pair.
-}

sortPairs :: Ord a1 => [(a2, a1)] -> [(a2, a1)]
sortPairs xs = sortBy (flip $ comparing snd) xs

{-
  3.3.
  - Define 'filename' that extracts the the name of the file from a file path.
    filename :: String -> String
    filename "/etc/init/cron.conf" => "cron.conf"
-}

filename :: String -> String
filename = reverse . takeWhile (/='/') . reverse

{-
  3.4.
  - Define 'maxElemIndices' that returns the indices of the maximum element in a
    list. Return "empty list" error if the list is empty.
    maxElemIndices :: Ord a => [a] -> [Int]
    maxElemIndices [1,3,4,1,3,4] => [2,5]
-}

maxElemIndices :: Ord a => [a] -> [Int]
maxElemIndices [] = error "empty list"
maxElemIndices xs = findIndices (==maxNum) xs
 where maxNum = maximum xs

-- EXERCISE 04 =======================================================================

{-
  4.1. 
  - Define 'elem'' using 'foldr'.
-}

elem' :: (Foldable t, Eq a) => a -> t a -> Bool
elem' e = foldr(\x acc -> e==x || acc) False

{-
  4.2.
  - Define 'reverse' using 'foldr'.
-}

reverse' :: [a] -> [a]
reverse' xs = foldr(\x acc -> acc++[x]) [] xs
  
{-
  4.3.
  - Using 'foldr' define 'nubRuns' that removes consecutively repeated elements
    from a list.
    nubRuns :: Eq a => [a] -> [a]
    nubRuns "Mississippi" => "Misisipi"
-}

nubRuns :: Eq a => [a] -> [a]
nubRuns xs = foldr(\x acc -> if x == (head acc) then acc else x:acc) [(last xs)] (init xs)

-- EXERCISE 05 =======================================================================

{-
  5.1.
  - Write 'reverse' using 'foldl'.
    reverse' :: [a] -> [a]
-}

reverse'' :: [a] -> [a]
reverse'' xs = foldl(\acc x -> x:acc) [] xs

{-
  5.2.
  - Using 'foldl' define function 'sumEven' from problem 1.1.
-}

sumEven' = foldl(\acc (x, y) -> if even x then acc + y else acc) 0 . zip [0..]

{-
  5.3.
  - Using 'foldl' define maxUnzip :: [(Int,Int)] -> (Int,Int) 
    that returns the maximum element at first position in a pair and maximum
    element at second position in the pair. In other words, the function should
    be equivalent to:
      maxUnzip zs = (maximum xs, maximum ys)
        where (xs,ys) = unzip zs
    Return "empty list" error if the list is empty.
-}

maxUnzip :: [(Int,Int)] -> (Int,Int) 
maxUnzip [] = error "error"
maxUnzip ((x,y):xs) = foldl(\(accX, accY) (x,y) -> (max accX x, max accY y)) (x,y) xs


{-LECTURE 09-} --
-- EXERCISE 01 =======================================================================

{-
  1.1.
  - Define a 'Date' structure with the appropriate fields.
  - Define a function that shows a date in the DD.MM.YYYY format (without
    leading zeroes).
    showDate :: Date -> String
-}
data Date = Date Integer Integer Integer
 deriving Show

showDate :: Date -> String
showDate (Date d m y) = (show d) ++ "." ++ (show m) ++ "." ++ (show y)

{-
  1.2.
  - Define a function
    translate :: Point -> Shape2 -> Shape2
    that translates a shape into the direction of vector (x,y).
-}

data Point = Point Double Double 
  deriving Show

data Shape2 = Circle2 Point Double | Rectangle2 Point Point 
  deriving Show

translate :: Point -> Shape2 -> Shape2  
translate (Point x y) (Circle2 (Point cx cy) r) = (Circle2 (Point (x + cx) (y + cy)) r)
translate (Point x y) (Rectangle2 (Point p1x p1y) (Point p2x p2y)) = (Rectangle2 (Point (p1x + x) (p1y + y)) (Point (p2x + x) (p2y + y)))

{-
  1.3.
  - Write a function 'inShape' that tests whether a point is contained within a
    given shape (or is on its border).
    inShape :: Shape2 -> Point -> Bool
  - Write a function 'inShapes' that tests if the point is within any shape from
    the list of shapes.
    inShapes :: [Shape2] -> Point -> Bool
-}

inShape :: Shape2 -> Point -> Bool
inShape (Circle2 (Point cx cy) r) (Point x y) = (cx - x)^2 + (cy - y)^2 <= r^2
inShape (Rectangle2 (Point p1x p1y) (Point p2x p2y)) (Point x y) = and[x >= min p1x p2x, x <= max p1x p2x, y >= min p1y p2y, y <= max p1y p2y]

inShapes :: [Shape2] -> Point -> Bool
inShapes xs p = foldr (\x a -> (inShape x p) || a) False xs

{-
  1.4.
  - Define your type 'Vehicle' that can be a 'Car', 'Truck', 
    'Motorcycle', or 'Bicycle'. The first three store a name of the manufacturer
    (String) and horsepower (Double).
  - Write a function 'totalHorsepower' that adds up the horsepower of the
    vehicles, assuming that bicycle's horsepower is 0.2.
-}

data Vehicle = Car String Double | Truck String Double | Motorcycle String Double | Bicycle deriving (Show)

totalHorsepower acc [] = acc
totalHorsepower acc ((Bicycle):xs) = totalHorsepower (acc + 0.2) xs
totalHorsepower acc ((Car _ h):xs) = totalHorsepower (acc + h) xs
totalHorsepower acc ((Truck _ h):xs) = totalHorsepower (acc + h) xs
totalHorsepower acc ((Motorcycle _ h):xs) = totalHorsepower (acc + h) xs

-- EXERCISE 02 =======================================================================

data Level = Bachelor | Master | PhD deriving (Show, Eq)

data Student = Student
  { firstName  :: String
  , lastName   :: String
  , studentId  :: String
  , level      :: Level
  , avgGrade   :: Double
  } deriving Show

{-
  2.1.
  - Define a function that increases the average grade of the student by 1.0,
    but not above 5.0.
    improveStudent :: Student -> Student
-}

improveStudent :: Student -> Student
improveStudent s
 | avgGrade s > 4.0 = s
 | otherwise = s {avgGrade = min (avgGrade s + 1.0) 5.0}

{-
  2.2.
  - Write a function to compute the average grade of students for the different
    study levels.
    avgGradePerLevels :: [Student] -> (Double, Double, Double)
-}

getAvg [] = 0
getAvg ls = (sum $ map (\s -> avgGrade s) ls) / (fromIntegral $ length ls)

filterBy l = filter (\s -> level s == l)

avgGradePerLevels :: [Student] -> (Double, Double, Double)
avgGradePerLevels ls = (getAvg $ (filterBy Bachelor) ls , getAvg $ (filterBy Master) ls , getAvg $ (filterBy PhD) ls)

{-
  2.3.
  - Write a function that returns a list of matriculation numbers for a given
    study level, sorted by average grade in descending order.
    rankedStudents :: Level -> [Student] -> [String]
-}

rankedStudents :: Level -> [Student] -> [String]
rankedStudents lvl ss = map (fst) $ sortBy (flip $ comparing snd) $ map(\x -> (studentId x, avgGrade x)) $ filter(\x -> level x == lvl) ss

{-
  2.4.
  - Write a function
    addStudent :: Student -> [Student] -> [Student]
    that adds a student to a list of students. If a student with an identical
    matriculation number already exists in the list, the function should return an
    error. 
-}

addStudent :: Student -> [Student] -> [Student]
addStudent s ss
 | length (filter(\x -> studentId s == studentId x) ss) > 0 = error "Error"
 | otherwise = s:ss

-- EXERCISE 03 =======================================================================

{-
  3.1.
  - Define your own parametrized type 'MyTriplet' that contains the values of
    three different types. Do this using a record.
  - Define a function 
    toTriplet :: MyTriplet a b c -> (a, b, c)
    that converts a 'MyTriplet' value into an ordinary triplet.
-}

data MyTriplet a b c = MyTriplet
 { myA :: a
 , myB :: b
 , myC :: c
 } deriving (Show)

toTriplet :: MyTriplet a b c -> (a, b, c)
toTriplet (MyTriplet a b c) = (a, b, c)

{-
  3.2.
  - Define a function (Employee - salary :: Maybe Double, name :: String) deriving Show
    totalSalaries :: [Employee] -> Double
    that sums the known salaries of employees (salaries that are not 'Nothing').
-}

data Employee = Employee
  { name   :: String
  , salary :: Maybe Double
  } deriving Show

totalSalaries :: [Employee] -> Double
totalSalaries = totalSalaries' 0

totalSalaries' acc [] = acc
totalSalaries' acc (em:es) = case salary em of
 Just e  -> totalSalaries' (acc + e) es
 Nothing -> totalSalaries' acc es
 
{-
  3.3.
  - Write a function 'addStudent2' that works like 'addStudent' from problem 2.4
    but returns a 'Maybe' type instead of an error.
    addStudent2 :: Student -> [Student] -> Maybe [Student]
  - Write 'addStudent3' that returns an 'Either'.
-}

addStudent2 :: Student -> [Student] -> Maybe [Student]
addStudent2 s ss
 | length (filter(\x -> studentId s == studentId x) ss) > 0 = Just ss
 | otherwise = Just (s:ss)

addStudent3 = undefined