module Homework where
--
import Data.List
import Data.Char
import Data.Bits ( xor )
--

-- Task 01
{-A local maximum of a list is an element of the list which is strictly
greater than both the elements immediately before and after it. For example, in the
list [2,3,4,1,5], the only local maximum is 4, since it is greater than the elements
immediately before and after it (3 and 1). The number 5 is not a local maximum
since there is no element that comes after it.
Write a function:
localMaxima :: [Int] -> [Int]
which finds all the local maxima in the input list and returns them in order. For
example:
localMaxima [2,9,5,6,1] ) [9,6]
localMaxima [2,3,4,1,5] ) [4]
localMaxima [1,2,3,4,5] ) []-}
localMaxima :: [Int] -> [Int]
localMaxima xs = reverse $ localMaxima' [] xs

localMaxima' ls [] = ls
localMaxima' ls (_:[]) = ls
localMaxima' ls (_:_:[]) = ls
localMaxima' ls (x:y:z:xs)
 | and[x < y, y > z] = localMaxima' (y:ls) (y:z:xs)
 | otherwise         = localMaxima' ls (y:z:xs)

-- Task 02
{-Scrabble is a word game in which two to four players score points by placing
tiles bearing a single letter onto a board divided into a 15x15 grid of squares. The
tiles must form words which, in crossword fashion, read left to right in rows or
downwards in columns, and be defined in a standard dictionary or lexicon.
In this problem you will have to extract the scrabble scores from a legacy system
and convert them to the new system.
The old system stored a list of letters per score:
[(Int, String)]
- 1 point: \AEIOULNRST"
- 2 points: \DG"
- 3 points: \BCMP"
- 4 points: \FHVWY"
- 5 points: \K"
- 8 points: \JX"
- 10 points: \QZ"
The shiny new scrabble system instead stores the score per letter, which makes it
much faster and easier to calculate the score for a word. It also stores the letters in
lower-case regardless of the case of the input letters:
[(Char, Int)]
- \a" is worth 1 point.
- \b" is worth 3 points.
- \c" is worth 3 points.
- \d" is worth 2 points.
There is no need to implement a function that will print out our list of (Char, Int)
tuples. This was just given as an example of how such value could be used.
Your task is to implement a function:
transform :: [(Int,String)] -> [(Char,Int)]-}

transform :: [(Int, String)] -> [(Char, Int)]
transform xs = transform' [] xs

transform' :: [(Char,Int)] -> [(Int,String)] -> [(Char, Int)]
transform' ls []            = ls
transform' ls ((_,[]):ys)   = transform' ls ys
transform' ls ((p,x:xs):ys) = transform' ((toLower x, p):ls) ((p, xs):ys)

-- Task 03
{-Wolfram code is a naming system often used for one-dimensional cellular
automaton rules, introduced by Stephen Wolfram.
The cellular automaton consists of a one-dimensional array of cells which can be
either on or off. Each code describes a set of rules which determines the next
configuration of cells based on their neighbours.
You will implement a simplified version of the automaton, which works on a fixed-size
array, and only implements the simple Rule 90, so you can hard code the behaviour
in instead of having to decode the rules. Please read the linked Wikipedia page for
more details.
Define a function:
rule90 :: [Bool] -> [[Bool]]
which takes the initial state of the array of cells and returns an infinite list of con-
secutive states of the array.
To help with defining the function, define:
rule90Step :: [Bool] -> [Bool]
which returns the next state of the array. The edges of the array are assumed to be
False.
And finally, define:
pretty :: [[Bool]] -> String
which returns a String where each line contains a state of the board, and where
True cells are represented with a '#' and False cells are represented with a space.
(You can find the xor function in the Data.Bits module. Alternatively, you can use
the inequality (/=) operator.)
An example:
ghci> putStr $ pretty $ take 8 $
rule90 (replicate 7 False ++ [True] ++ replicate 7 False)-}

rule90 :: [Bool] -> [[Bool]]
rule90 xs = xs : (rule90 rs)
 where rs = rule90Step xs

rule90Step :: [Bool] -> [Bool]
rule90Step res@(x:y:xs) = rule90Step' [y] res

rule90Step' ls (x:[])     = ls
rule90Step' ls (x:y:[])   = x:ls
rule90Step' ls (x:y:z:xs) = rule90Step' ((x `xor` z):ls) (y:z:xs)

pretty :: [[Bool]] -> String
pretty xs = concat $ map prettyList xs

prettyList xs = '\n' : (map (\x -> if x then '#' else ' ') xs)
